# NORAX
[![N|Solid](https://cdn.discordapp.com/attachments/375305492404371456/829470397279895562/noraxlogocrop.png)](https://gitlab.com/magshimim-markez-2021/06/603/norax)
## _A software application for remote control_

Norax is a software application for remote control that you and your friends can help each other.

- Select the window you want to allow the other user to control.
- Send him the code which is shown on the page.
- The other user will have to enter the code and he will have control over your PC.

## Tech

Norax uses a number of open source projects to work properly:

- [KeyboardEvent] - Objects describe a user interaction with the keyboard.
- [MouseEvent] - Events that occur when the mouse interacts with the HTML document belongs to the MouseEvent Object.
- [Robotjs] - Control the mouse and keyboard.
- [Peerjs] - PeerJS simplifies WebRTC peer-to-peer data, video, and audio calls.
- [node.js] - As an asynchronous event-driven JavaScript runtime, Node.js is designed to build scalable network applications.
- [Electronjs] - Build cross-platform desktop apps with JavaScript, HTML, and CSS

## Installation for developers

Norax requires [Node.js](https://nodejs.org/).

Install the dependencies and devDependencies and start the client.

```sh
git clone https://gitlab.com/magshimim-markez-2021/06/603/norax.git
cd norax
cd mainClient
npm i
npm start
```

## Installation for users

Norax requires [Node.js](https://nodejs.org/).

Download the [v1.0.3] release from GitLab go to setup and run NORAX-1.0.3 Setup.exe

## Development

## License

Apache License Version 2.0

**Free Software**

[//]: #

   [Electronjs]: <https://www.electronjs.org/>
   [robotjs]: <http://robotjs.io/>
   [MouseEvent]: <https://www.w3schools.com/jsref/obj_mouseevent.asp>
   [node.js]: <http://nodejs.org>
   [Peerjs]: <https://peerjs.com/>
   [express]: <http://expressjs.com>
   [KeyboardEvent]: <https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent>
   [v1.0.3]: <https://gitlab.com/Nir.B/norax/-/releases/v1.0.3>
