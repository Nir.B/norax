var ipc = require('electron').ipcRenderer;

/* HTML elements */

var controlBtn = document.getElementById('controlBtn');
var controlledBtn = document.getElementById('controlledBtn');
controlBtn.onclick = openControlClient;
controlledBtn.onclick = openControlledClient;

/* */

// Open the control client window
function openControlClient() {
    ipc.send('openControlClient');
}

// Open the controlled client window
function openControlledClient() {
    ipc.send('openControlledClient');
}