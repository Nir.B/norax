const { app, BrowserWindow } = require('electron');
var ipc = require('electron').ipcMain;
const path = require('path');
var mainWindow;

/* Used and modified part of the code from https://www.electronjs.org/docs/tutorial/quick-start */

if (require('electron-squirrel-startup')) {
  app.quit();
}

const createWindow = () => {
  mainWindow = new BrowserWindow({
    width: 1280,
    height: 800,
    minWidth: 720,
    minHeight: 800,
    webPreferences: {
      nodeIntegration: true,
      devTools: false
    },
    icon: path.join(__dirname, 'Logo/noraxpngblue.ico')
  });
  mainWindow.loadFile(path.join(__dirname, 'mainPage.html'));
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('child-process-gone', () => {
  mainWindow.show();
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

/* */

// Open the control client window
ipc.on('openControlClient', function (event, data) {
  mainWindow.loadFile(path.join(__dirname, 'controlClient.html'));
});

// Open the controlled client window
ipc.on('openControlledClient', function (event, data) {
  mainWindow.loadFile(path.join(__dirname, 'controlledClient.html'));
});

// Open the main page window
ipc.on('backToMenu', function (event, data) {
  mainWindow.loadFile(path.join(__dirname, 'mainPage.html'));
});